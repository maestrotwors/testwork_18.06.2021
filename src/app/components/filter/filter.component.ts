import { Component, OnInit } from '@angular/core';
import { StoreService } from '../../services/store.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {

  constructor(public store: StoreService ) { }

  ngOnInit(): void {
  }

  public close_tag(id){
    alert(id);
  }

  public sendData(){
    let dataToSend = new Array<object>();
    this.store.data_to_send = [];

    Object.entries(this.store.paramsToSend).forEach(([key, value])=>{
      if (value>'' && value!= false){
        dataToSend.push({id: key, 'value': value});
      }
    });

    for (let value of Object.values(this.store.categoriesChildren)) {
      value['items'].forEach(element => {
        if(element['selected']){ dataToSend.push({id: element['id'], 'value':true})}
      });
    }

    this.store.data_to_send= dataToSend;
  }

}
