
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { notfoundComponent } from './notfound.component';
import { NotfoundRoutingModule } from './notfound-routing.module';

@NgModule({
  imports: [
    CommonModule,
    NotfoundRoutingModule,
    FormsModule,
  ],
  exports: [
  ],
  declarations: [
    notfoundComponent
  ],
  providers: [

  ]
})
export class NotfoundModule { }

