
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { notfoundComponent } from './notfound.component';

const routes: Routes = [
  { path: '', component: notfoundComponent },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class NotfoundRoutingModule { }
