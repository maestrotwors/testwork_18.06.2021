
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { IndexComponent } from './index.component';
import { IndexRoutingModule } from './index-routing.module';

@NgModule({
  imports: [
    CommonModule,
    IndexRoutingModule,
    FormsModule,
  ],
  exports: [
  ],
  declarations: [
    IndexComponent
  ],
  providers: [

  ]
})
export class IndexModule { }

