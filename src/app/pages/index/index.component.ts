import { Component, AfterViewInit} from '@angular/core';
import { StoreService } from '../../services/store.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  providers: [],
})

export class IndexComponent implements AfterViewInit {

  constructor(public store: StoreService){}

  ngAfterViewInit(){

  }

}

