import {Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StoreService } from './store.service';

@Injectable({
  providedIn: 'root',
})
export class HttpService {

  constructor(public http_client: HttpClient, public store: StoreService ) {}

  public getData() {
    this.http_client.get('https://api.npoint.io/b45f5662daab7f1b7fd4').subscribe((data) => {
      //let x = this.store.data.subscribe(x =>{});
      this.store.data.next(data['items']);
      let categories = data['items'].filter(s => s['countChildren']>0).map( x => {
        return { id: x['id'], name: x['name'], isOpen: false}
      });
      this.store.categories.next(categories);

      let data_cat_array = [];
      data['items'].forEach(function (el) {
        let items = new Array;
        if(el['parentCategoryId']>""){
          if(data_cat_array[el['parentCategoryId']]!=undefined){
            items = [];
            items = data_cat_array[el['parentCategoryId']];
            items["items"].push({"id": el['id'], "name": el['name'], selected: false});
            data_cat_array[el['parentCategoryId']] = items;
          }else{
            data_cat_array[el['parentCategoryId']] = {items: [{id: el['id'], name: el['name'], selected: false}]};
          }
        }else{
        }
      });

      this.store.categoriesChildren = data_cat_array ;
    });
  }

  public send_request(){

  }

}
