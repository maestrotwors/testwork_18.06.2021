import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AsyncSubject, Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class StoreService {

  constructor() {
    this.data = new Subject<object[]>();
    this.categories = new Subject<object[]>();
    this.categoriesFilter = "";
    this.categoriesChildren = new Array<object>();
    this.paramsToSend = [];
    this.paramsToSendCat = [];
    this.data_to_send = [];
  }

  public data: Subject<object[]>;
  public categories: Subject<object[]>;
  public categoriesFilter;
  public categoriesChildren: Array<object>;
  public paramsToSendCat: any[];
  public paramsToSend: any[];
  public data_to_send: object[];

  public get getSelectedCategories(){
    return [{id:1,name:'1nameeee'},{id:2,name:'2nameeee'},{id:3,name:'3nameeee'}];
  }
}
