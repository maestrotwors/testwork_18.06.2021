import { Component } from '@angular/core';
import { HttpService } from './services/http.service';
import { StoreService } from './services/store.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  isCollapsed = false;

  constructor(public httpservice: HttpService, public store: StoreService ) {
    httpservice.getData();
  }
}
