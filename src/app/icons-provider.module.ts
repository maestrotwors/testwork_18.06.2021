import { NgModule } from '@angular/core';
import { NZ_ICONS, NzIconModule } from 'ng-zorro-antd/icon';

import {
  MenuFoldOutline,
  MenuUnfoldOutline,
  FormOutline,
  DashboardOutline
} from '@ant-design/icons-angular/icons';

const icons = [MenuFoldOutline, MenuUnfoldOutline, DashboardOutline, FormOutline];

@NgModule({
  imports: [NzIconModule],
  exports: [NzIconModule],
  providers: [
    { provide: NZ_ICONS, useValue: icons }
  ]
})
export class IconsProviderModule {
}

/*

Когда преподаватель Владимир Валентинович объявит о практике, то студенты оффлайн направления 9 набора перемещаются в другую комнату ЗУМ для практики:

Практика для 9 набора: https://us02web.zoom.us/j/88311289981
пароль: 9165521

*/
